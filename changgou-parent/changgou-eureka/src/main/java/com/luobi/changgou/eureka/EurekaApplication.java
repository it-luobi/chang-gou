package com.luobi.changgou.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer  // 声明当前应用为Eureka服务
@SpringBootApplication
public class EurekaApplication {

    // http://127.0.0.1:6868
    public static void main(String[] args) {
        SpringApplication.run(EurekaApplication.class);
    }

}
