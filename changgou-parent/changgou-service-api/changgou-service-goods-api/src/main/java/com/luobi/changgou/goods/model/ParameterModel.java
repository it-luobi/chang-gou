package com.luobi.changgou.goods.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Table(name = "tb_para")
public class ParameterModel implements Serializable {
    
    @Id
    private Integer id;          //  id
    private String name;         //  名称
    private String options;      //  选项
    @Column(name = "seq")
    private Integer sort;        //  排序
    private Integer templateId;  //  模板ID
    
}
