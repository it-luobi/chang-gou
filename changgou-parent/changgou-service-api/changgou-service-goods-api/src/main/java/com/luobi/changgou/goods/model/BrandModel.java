package com.luobi.changgou.goods.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Table(name = "tb_brand")
public class BrandModel implements Serializable {

    @Id
    private Integer id;     // 品牌id
    private String name;    // 品牌名称
    private String image;   // 品牌图片地址
    private String letter;  // 品牌的首字母
    @Column(name = "seq")
    private Integer sort;   // 排序

}
