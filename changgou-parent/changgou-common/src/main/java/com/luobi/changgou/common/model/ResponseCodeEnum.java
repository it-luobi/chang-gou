package com.luobi.changgou.common.model;

import lombok.Getter;

@Getter
public enum ResponseCodeEnum {

    /**
     * 0 -> success
     */
    SUCCESS(0),

    /**
     * 100-999 -> common error
     */
    RESOURCE_EXISTS(101),
    RESOURCE_NOT_EXISTS(102),

    PARAMETER_IS_NULL(201),
    PARAMETER_IS_INVALID(202),

    INVALID_OPERATION(301),
    OPERATION_FAILED(302),
    DUPLICATE_KEY(303),
    OUT_OF_BOUND(304),

    NO_AUTH(401),

    SYSTEM_ERROR(500),
    REJECT_OPERATION(999),

    /**
     * 1000-9999 -> API ERROR
     */
    API_ERROR(1000),
    NOT_SUPPORTED_API(1001),
    ;

    private Integer code;

    ResponseCodeEnum(Integer code) {
        this.code = code;
    }

}
