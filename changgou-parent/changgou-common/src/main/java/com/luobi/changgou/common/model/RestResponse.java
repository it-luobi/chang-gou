package com.luobi.changgou.common.model;

import lombok.Data;

@Data
public class RestResponse<T> {
	private boolean success;
	private ResponseCodeEnum code;
	private T result;
	private String message;
	private String errorCode;

	public RestResponse(T result) {
		this.success = true;
		this.code = ResponseCodeEnum.SUCCESS;
		this.result = result;
	}

	public RestResponse(ResponseCodeEnum code, String message) {
		this.success = false;
		this.code = code;
		this.message = message;
	}

	public RestResponse(ResponseCodeEnum code, String message, T data) {
		this.success = false;
		this.code = code;
		this.message = message;
		this.result = data;
	}

	public RestResponse() {
		this.success = true;
		this.code = ResponseCodeEnum.SUCCESS;
	}

	public RestResponse(boolean success, ResponseCodeEnum code, String message, T result, String errorCode) {
		this.success = success;
		this.code = code;
		this.message = message;
		this.result = result;
		this.errorCode = errorCode;
	}

	public RestResponse(boolean success, ResponseCodeEnum code, String message, String errorCode) {
		this.success = success;
		this.code = code;
		this.message = message;
		this.errorCode = errorCode;
	}

	public static <K> RestResponse<K> success(K data) {
		return new RestResponse<>(data);
	}

	public static <K> RestResponse<K> success() {
		return new RestResponse<>();
	}

	public static <K> RestResponse<K> success(boolean success, ResponseCodeEnum code, String message, K result, String errorCode) {
		return new RestResponse<>(success, code, message, result, errorCode);
	}

	public static <K> RestResponse<K> fail(String errorCode, String message) {
		return new RestResponse<>(false, ResponseCodeEnum.OPERATION_FAILED, message, errorCode);
	}

	public static <K> RestResponse<K> fail(ResponseCodeEnum code, String message) {
		return new RestResponse<>(code, message);
	}

	public static <K> RestResponse<K> fail(ResponseCodeEnum code, K result) {
		return new RestResponse<>(code, "", result);
	}

	public static <K> RestResponse<K> fail(ResponseCodeEnum code, String message, K result) {
		return new RestResponse<>(code, message, result);
	}

	public static <K> RestResponse<K> fail(ResponseCodeEnum code) {
		return new RestResponse<>(code, code.name());
	}

}
