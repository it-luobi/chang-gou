package com.luobi.changgou.common.model;

import lombok.Data;

import java.util.Collections;
import java.util.List;

@Data
public class PageData<T> {
    private long total;
    private int pageSize;
    private int pageNo;
    private List<T> data;

    public PageData() {
        this(Collections.emptyList());
    }

    public PageData(PageRequest pageRequest) {
        this(Collections.emptyList(), pageRequest, 0);
    }

    public PageData(List<T> data, PageRequest pageRequest, long total) {
        this.data = data;
        this.pageNo = getPageNo(pageRequest);
        this.pageSize = getPageSize(pageRequest);
        this.total = total;
    }

    public PageData(List<T> data, int pageNo, int pageSize, long total) {
        this.data = data;
        this.pageNo = pageNo;
        this.pageSize = pageSize;
        this.total = total;
    }

    public PageData(List<T> data) {
        this(data, null, null == data ? 0 : data.size());
    }

    public PageData(int pageNo, int pageSize) {
        super();
        this.pageSize = pageSize;
        this.pageNo = pageNo;
    }

    private int getPageNo(PageRequest pageRequest) {
        return pageRequest == null ? 1 : pageRequest.getPageNo();
    }

    private int getPageSize(PageRequest pageRequest) {
        return pageRequest == null ? Integer.MAX_VALUE : pageRequest.getPageSize();
    }

}
