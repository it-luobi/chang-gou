package com.luobi.changgou.goods.dto;

import com.luobi.changgou.goods.model.SpecificationModel;
import lombok.Data;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Data
public class SpecificationDto {

    private Integer id;
    private String name;
    private List<String> optionList;

    public static SpecificationDto convertModelToDto(SpecificationModel model) {
        if (Objects.isNull(model)) {
            return null;
        }
        SpecificationDto dto = new SpecificationDto();
        dto.setId(model.getId());
        dto.setName(model.getName());
        List<String> optionList = Optional.ofNullable(model.getOptions())
                .map(options -> Arrays.asList(options.split(",")))
                .orElse(Collections.emptyList());
        dto.setOptionList(optionList);
        return dto;
    }

}
