package com.luobi.changgou.goods.service;

import com.luobi.changgou.common.model.PageData;
import com.luobi.changgou.common.model.PageRequest;
import com.luobi.changgou.goods.dto.SpecificationDto;
import com.luobi.changgou.goods.dto.SpecificationQueryDto;
import com.luobi.changgou.goods.model.SpecificationModel;

import java.util.List;

public interface SpecificationService {

    /**
     * 查询所有规格列表
     */
    List<SpecificationModel> listAllSpecifications();

    /**
     * 根据id查询规格数据
     */
    SpecificationModel getSpecificationById(Integer id);

    /**
     * 规格列表条件查询
     */
    List<SpecificationModel> listSpecificationByCondition(SpecificationQueryDto SpecificationDto);

    /**
     * 规格列表分页查询
     */
    PageData<SpecificationModel> listSpecificationWithPageCondition(PageRequest pageRequest);

    /**
     * 规格列表分页+条件查询
     */
    PageData<SpecificationModel> searchSpecification(SpecificationQueryDto SpecificationDto, PageRequest pageRequest);

    /**
     * 新增规格
     */
    void addSpecification(SpecificationModel Specification);

    /**
     * 修改规格
     */
    void updateSpecification(SpecificationModel Specification);

    /**
     * 删除规格
     */
    void deleteSpecificationById(Integer id);

    /**
     * 根据商品分类名称查询规格列表
     */
    List<SpecificationDto> listSpecificationByCategoryName(String categoryName);

}
