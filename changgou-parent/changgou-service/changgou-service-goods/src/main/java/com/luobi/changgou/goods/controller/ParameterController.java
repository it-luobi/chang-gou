package com.luobi.changgou.goods.controller;

import com.luobi.changgou.common.model.PageData;
import com.luobi.changgou.common.model.PageRequest;
import com.luobi.changgou.common.model.RestResponse;
import com.luobi.changgou.goods.dto.ParameterQueryDto;
import com.luobi.changgou.goods.model.ParameterModel;
import com.luobi.changgou.goods.service.ParameterService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/parameter")
public class ParameterController {

    private final ParameterService parameterService;

    /**
     * 查询所有参数列表
     */
    @GetMapping("/all")
    public RestResponse<List<ParameterModel>> listAllParameters() {
        List<ParameterModel> allParameters = parameterService.listAllParameters();
        return RestResponse.success(allParameters);
    }

    /**
     * 根据id查询参数数据
     */
    @GetMapping("/{id}")
    public RestResponse<ParameterModel> getParameterById(@PathVariable Integer id) {
        return RestResponse.success(parameterService.getParameterById(id));
    }

    /**
     * 参数列表条件查询
     */
    @GetMapping("/condition")
    public RestResponse<List<ParameterModel>> listParameterByCondition(ParameterQueryDto queryDto) {
        return RestResponse.success(parameterService.listParameterByCondition(queryDto));
    }

    /**
     * 参数列表分页查询
     */
    @GetMapping("/page")
    public RestResponse<PageData<ParameterModel>> listParameterWithPageCondition(PageRequest pageRequest) {
        return RestResponse.success(parameterService.listParameterWithPageCondition(pageRequest));
    }

    /**
     * 参数列表分页+条件查询
     */
    @GetMapping("/search")
    public RestResponse<PageData<ParameterModel>> searchParameter(ParameterQueryDto queryDto, PageRequest pageRequest) {
        return RestResponse.success(parameterService.searchParameter(queryDto, pageRequest));
    }

    /**
     * 新增参数
     */
    @PostMapping("/add")
    public RestResponse<?> addParameter(@RequestBody ParameterModel parameter) {
        parameterService.addParameter(parameter);
        return RestResponse.success();
    }

    /**
     * 修改参数
     */
    @PutMapping("/update")
    public RestResponse<?> updateParameter(@RequestBody ParameterModel parameter) {
        parameterService.updateParameter(parameter);
        return RestResponse.success();
    }

    /**
     * 删除参数
     */
    @DeleteMapping("/{id}")
    public RestResponse<?> deleteParameterById(@PathVariable Integer id) {
        parameterService.deleteParameterById(id);
        return RestResponse.success();
    }

}
