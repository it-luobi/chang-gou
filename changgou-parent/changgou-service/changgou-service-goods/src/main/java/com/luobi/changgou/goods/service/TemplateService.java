package com.luobi.changgou.goods.service;

import com.luobi.changgou.common.model.PageData;
import com.luobi.changgou.common.model.PageRequest;
import com.luobi.changgou.goods.dto.TemplateQueryDto;
import com.luobi.changgou.goods.model.TemplateModel;

import java.util.List;

public interface TemplateService {

    /**
     * 查询所有模版列表
     */
    List<TemplateModel> listAllTemplates();

    /**
     * 根据id查询模版数据
     */
    TemplateModel getTemplateById(Integer id);

    /**
     * 模版列表条件查询
     */
    List<TemplateModel> listTemplateByCondition(TemplateQueryDto TemplateDto);

    /**
     * 模版列表分页查询
     */
    PageData<TemplateModel> listTemplateWithPageCondition(PageRequest pageRequest);

    /**
     * 模版列表分页+条件查询
     */
    PageData<TemplateModel> searchTemplate(TemplateQueryDto TemplateDto, PageRequest pageRequest);

    /**
     * 新增模版
     */
    void addTemplate(TemplateModel Template);

    /**
     * 修改模版
     */
    void updateTemplate(TemplateModel Template);

    /**
     * 删除模版
     */
    void deleteTemplateById(Integer id);

}
