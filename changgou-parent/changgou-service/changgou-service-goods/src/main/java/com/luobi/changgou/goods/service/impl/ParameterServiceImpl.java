package com.luobi.changgou.goods.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.luobi.changgou.common.model.PageData;
import com.luobi.changgou.common.model.PageRequest;
import com.luobi.changgou.goods.dao.ParameterMapper;
import com.luobi.changgou.goods.dto.ParameterQueryDto;
import com.luobi.changgou.goods.model.ParameterModel;
import com.luobi.changgou.goods.service.ParameterService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class ParameterServiceImpl implements ParameterService {

    private final ParameterMapper ParameterMapper;

    @Override
    public List<ParameterModel> listAllParameters() {
        return ParameterMapper.selectAll();
    }

    @Override
    public ParameterModel getParameterById(Integer id) {
        return ParameterMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<ParameterModel> listParameterByCondition(ParameterQueryDto queryDto) {
        Example example = new Example(ParameterModel.class);
        Example.Criteria criteria = example.createCriteria();
        if (StringUtils.isNotBlank(queryDto.getName())) {
            criteria.andLike("name", "%" + queryDto.getName() + "%");
        }
        if (StringUtils.isNotBlank(queryDto.getOptions())) {
            criteria.andEqualTo("options", queryDto.getOptions());
        }
        if (Objects.nonNull(queryDto.getTemplateId())) {
            criteria.andEqualTo("templateId", queryDto.getTemplateId());
        }
        return ParameterMapper.selectByExample(example);
    }

    @Override
    public PageData<ParameterModel> listParameterWithPageCondition(PageRequest pageRequest) {
        PageHelper.startPage(pageRequest.getPageNo(), pageRequest.getPageSize());
        Page<ParameterModel> page = (Page<ParameterModel>) ParameterMapper.selectAll();
        return new PageData<>(page.getResult(), pageRequest, page.getTotal());
    }

    @Override
    public PageData<ParameterModel> searchParameter(ParameterQueryDto queryDto, PageRequest pageRequest) {
        Example example = new Example(ParameterModel.class);
        Example.Criteria criteria = example.createCriteria();
        if (StringUtils.isNotBlank(queryDto.getName())) {
            criteria.andLike("name", "%" + queryDto.getName() + "%");
        }
        if (StringUtils.isNotBlank(queryDto.getOptions())) {
            criteria.andEqualTo("options", queryDto.getOptions());
        }
        if (Objects.nonNull(queryDto.getTemplateId())) {
            criteria.andEqualTo("templateId", queryDto.getTemplateId());
        }

        PageHelper.startPage(pageRequest.getPageNo(), pageRequest.getPageSize());
        Page<ParameterModel> page = (Page<ParameterModel>) ParameterMapper.selectByExample(example);
        return new PageData<>(page.getResult(), pageRequest, page.getTotal());
    }

    @Override
    public void addParameter(ParameterModel parameter) {
        ParameterMapper.insertSelective(parameter);
    }

    @Override
    public void updateParameter(ParameterModel parameter) {
        ParameterMapper.updateByPrimaryKeySelective(parameter);
    }

    @Override
    public void deleteParameterById(Integer id) {
        ParameterMapper.deleteByPrimaryKey(id);
    }

}
