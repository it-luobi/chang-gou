package com.luobi.changgou.goods.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.luobi.changgou.common.model.PageData;
import com.luobi.changgou.common.model.PageRequest;
import com.luobi.changgou.goods.dao.SpecificationMapper;
import com.luobi.changgou.goods.dto.SpecificationDto;
import com.luobi.changgou.goods.dto.SpecificationQueryDto;
import com.luobi.changgou.goods.model.SpecificationModel;
import com.luobi.changgou.goods.service.SpecificationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class SpecificationServiceImpl implements SpecificationService {

    private final SpecificationMapper specificationMapper;

    @Override
    public List<SpecificationModel> listAllSpecifications() {
        return specificationMapper.selectAll();
    }

    @Override
    public SpecificationModel getSpecificationById(Integer id) {
        return specificationMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<SpecificationModel> listSpecificationByCondition(SpecificationQueryDto queryDto) {
        Example example = new Example(SpecificationModel.class);
        Example.Criteria criteria = example.createCriteria();
        if (StringUtils.isNotBlank(queryDto.getName())) {
            criteria.andLike("name", "%" + queryDto.getName() + "%");
        }
        if (StringUtils.isNotBlank(queryDto.getOptions())) {
            criteria.andEqualTo("options", queryDto.getOptions());
        }
        if (Objects.nonNull(queryDto.getTemplateId())) {
            criteria.andEqualTo("templateId", queryDto.getTemplateId());
        }
        return specificationMapper.selectByExample(example);
    }

    @Override
    public PageData<SpecificationModel> listSpecificationWithPageCondition(PageRequest pageRequest) {
        PageHelper.startPage(pageRequest.getPageNo(), pageRequest.getPageSize());
        Page<SpecificationModel> page = (Page<SpecificationModel>) specificationMapper.selectAll();
        return new PageData<>(page.getResult(), pageRequest, page.getTotal());
    }

    @Override
    public PageData<SpecificationModel> searchSpecification(SpecificationQueryDto queryDto, PageRequest pageRequest) {
        Example example = new Example(SpecificationModel.class);
        Example.Criteria criteria = example.createCriteria();
        if (StringUtils.isNotBlank(queryDto.getName())) {
            criteria.andLike("name", "%" + queryDto.getName() + "%");
        }
        if (StringUtils.isNotBlank(queryDto.getOptions())) {
            criteria.andEqualTo("options", queryDto.getOptions());
        }
        if (Objects.nonNull(queryDto.getTemplateId())) {
            criteria.andEqualTo("templateId", queryDto.getTemplateId());
        }

        PageHelper.startPage(pageRequest.getPageNo(), pageRequest.getPageSize());
        Page<SpecificationModel> page = (Page<SpecificationModel>) specificationMapper.selectByExample(example);
        return new PageData<>(page.getResult(), pageRequest, page.getTotal());
    }

    @Override
    public void addSpecification(SpecificationModel specification) {
        specificationMapper.insertSelective(specification);
    }

    @Override
    public void updateSpecification(SpecificationModel specification) {
        specificationMapper.updateByPrimaryKeySelective(specification);
    }

    @Override
    public void deleteSpecificationById(Integer id) {
        specificationMapper.deleteByPrimaryKey(id);
    }

    /**
     * 根据类别名称列出所有规格。
     *
     * @param categoryName 类别名称
     * @return 规格DTO列表
     */
    @Override
    public List<SpecificationDto> listSpecificationByCategoryName(String categoryName) {
        List<SpecificationModel> list = specificationMapper.listSpecificationByCategoryName(categoryName);
        if (CollectionUtils.isEmpty(list)) {
            return Collections.emptyList();
        }
        return list.stream()
                .map(SpecificationDto::convertModelToDto)
                .collect(Collectors.toList());
    }

}
