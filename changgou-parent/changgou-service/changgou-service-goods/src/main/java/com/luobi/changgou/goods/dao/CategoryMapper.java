package com.luobi.changgou.goods.dao;

import com.luobi.changgou.goods.pojo.Category;
import tk.mybatis.mapper.common.Mapper;

public interface CategoryMapper extends Mapper<Category> {

}
