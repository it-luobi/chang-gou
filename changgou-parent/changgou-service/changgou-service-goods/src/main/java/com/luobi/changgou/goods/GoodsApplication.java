package com.luobi.changgou.goods;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@EnableEurekaClient
@MapperScan(basePackages = {"com.luobi.changgou.goods.dao"})
public class GoodsApplication {

    // http://localhost:9011/goods/swagger-ui/index.html
    public static void main(String[] args) {
        SpringApplication.run(GoodsApplication.class);
    }

}
