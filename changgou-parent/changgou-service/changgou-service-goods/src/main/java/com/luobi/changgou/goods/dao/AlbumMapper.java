package com.luobi.changgou.goods.dao;

import com.luobi.changgou.goods.pojo.Album;
import tk.mybatis.mapper.common.Mapper;

public interface AlbumMapper extends Mapper<Album> {

}
