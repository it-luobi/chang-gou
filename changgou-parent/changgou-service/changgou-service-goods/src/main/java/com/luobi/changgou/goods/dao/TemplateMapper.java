package com.luobi.changgou.goods.dao;

import com.luobi.changgou.goods.model.TemplateModel;
import tk.mybatis.mapper.common.Mapper;

public interface TemplateMapper extends Mapper<TemplateModel> {
}
