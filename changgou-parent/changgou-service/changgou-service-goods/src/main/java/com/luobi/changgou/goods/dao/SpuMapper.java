package com.luobi.changgou.goods.dao;

import com.luobi.changgou.goods.pojo.Spu;
import tk.mybatis.mapper.common.Mapper;

public interface SpuMapper extends Mapper<Spu> {

}
