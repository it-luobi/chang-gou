package com.luobi.changgou.goods.controller;

import com.luobi.changgou.common.model.PageData;
import com.luobi.changgou.common.model.PageRequest;
import com.luobi.changgou.common.model.RestResponse;
import com.luobi.changgou.goods.dto.SpecificationDto;
import com.luobi.changgou.goods.dto.SpecificationQueryDto;
import com.luobi.changgou.goods.model.SpecificationModel;
import com.luobi.changgou.goods.service.SpecificationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/specification")
public class SpecificationController {

    private final SpecificationService specificationService;

    /**
     * 查询所有规格列表
     */
    @GetMapping("/all")
    public RestResponse<List<SpecificationModel>> listAllSpecifications() {
        List<SpecificationModel> allSpecifications = specificationService.listAllSpecifications();
        return RestResponse.success(allSpecifications);
    }

    /**
     * 根据id查询规格数据
     */
    @GetMapping("/{id}")
    public RestResponse<SpecificationModel> getSpecificationById(@PathVariable Integer id) {
        return RestResponse.success(specificationService.getSpecificationById(id));
    }

    /**
     * 规格列表条件查询
     */
    @GetMapping("/condition")
    public RestResponse<List<SpecificationModel>> listSpecificationByCondition(SpecificationQueryDto queryDto) {
        return RestResponse.success(specificationService.listSpecificationByCondition(queryDto));
    }

    /**
     * 规格列表分页查询
     */
    @GetMapping("/page")
    public RestResponse<PageData<SpecificationModel>> listSpecificationWithPageCondition(PageRequest pageRequest) {
        return RestResponse.success(specificationService.listSpecificationWithPageCondition(pageRequest));
    }

    /**
     * 规格列表分页+条件查询
     */
    @GetMapping("/search")
    public RestResponse<PageData<SpecificationModel>> searchSpecification(SpecificationQueryDto queryDto, PageRequest pageRequest) {
        return RestResponse.success(specificationService.searchSpecification(queryDto, pageRequest));
    }

    /**
     * 新增规格
     */
    @PostMapping("/add")
    public RestResponse<?> addSpecification(@RequestBody SpecificationModel specification) {
        specificationService.addSpecification(specification);
        return RestResponse.success();
    }

    /**
     * 修改规格
     */
    @PutMapping("/update")
    public RestResponse<?> updateSpecification(@RequestBody SpecificationModel specification) {
        specificationService.updateSpecification(specification);
        return RestResponse.success();
    }

    /**
     * 删除规格
     */
    @DeleteMapping("/{id}")
    public RestResponse<?> deleteSpecificationById(@PathVariable Integer id) {
        specificationService.deleteSpecificationById(id);
        return RestResponse.success();
    }

    /**
     * 根据商品分类名称查询规格列表
     */
    @GetMapping("/list-by-category/{categoryName}")
    public RestResponse<List<SpecificationDto>> listSpecificationByCategoryName(@PathVariable String categoryName) {
        return RestResponse.success(specificationService.listSpecificationByCategoryName(categoryName));
    }

}
