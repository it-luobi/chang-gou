package com.luobi.changgou.goods.controller;

import com.luobi.changgou.common.model.PageData;
import com.luobi.changgou.common.model.PageRequest;
import com.luobi.changgou.common.model.RestResponse;
import com.luobi.changgou.goods.dto.BrandQueryDto;
import com.luobi.changgou.goods.model.BrandModel;
import com.luobi.changgou.goods.service.BrandService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/brand")
public class BrandController {

    private final BrandService brandService;

    /**
     * 查询所有品牌列表
     */
    @GetMapping("/all")
    public RestResponse<List<BrandModel>> listAllBrands() {
        List<BrandModel> allBrands = brandService.listAllBrands();
        return RestResponse.success(allBrands);
    }

    /**
     * 根据id查询品牌数据
     */
    @GetMapping("/{id}")
    public RestResponse<BrandModel> getBrandById(@PathVariable Integer id) {
        return RestResponse.success(brandService.getBrandById(id));
    }

    /**
     * 品牌列表条件查询
     */
    @GetMapping("/condition")
    public RestResponse<List<BrandModel>> listBrandByCondition(BrandQueryDto brandDto) {
        return RestResponse.success(brandService.listBrandByCondition(brandDto));
    }

    /**
     * 品牌列表分页查询
     */
    @GetMapping("/page")
    public RestResponse<PageData<BrandModel>> listBrandWithPageCondition(PageRequest pageRequest) {
        return RestResponse.success(brandService.listBrandWithPageCondition(pageRequest));
    }

    /**
     * 品牌列表分页+条件查询
     */
    @GetMapping("/search")
    public RestResponse<PageData<BrandModel>> searchBrand(BrandQueryDto brandDto, PageRequest pageRequest) {
        return RestResponse.success(brandService.searchBrand(brandDto, pageRequest));
    }

    /**
     * 新增品牌
     */
    @PostMapping("/add")
    public RestResponse<?> addBrand(@RequestBody BrandModel brand) {
        brandService.addBrand(brand);
        return RestResponse.success();
    }

    /**
     * 修改品牌
     */
    @PutMapping("/update")
    public RestResponse<?> updateBrand(@RequestBody BrandModel brand) {
        brandService.updateBrand(brand);
        return RestResponse.success();
    }

    /**
     * 删除品牌
     */
    @DeleteMapping("/{id}")
    public RestResponse<?> deleteBrandById(@PathVariable Integer id) {
        brandService.deleteBrandById(id);
        return RestResponse.success();
    }

    /**
     * 根据商品分类名称查询品牌列表
     */
    @GetMapping("/list-by-category/{categoryName}")
    public RestResponse<List<BrandModel>> listBrandByCategoryName(@PathVariable String categoryName) {
        return RestResponse.success(brandService.listBrandByCategoryName(categoryName));
    }

}
