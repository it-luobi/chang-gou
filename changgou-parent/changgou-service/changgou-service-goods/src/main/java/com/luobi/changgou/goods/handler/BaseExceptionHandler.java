package com.luobi.changgou.goods.handler;

import com.luobi.changgou.common.model.ResponseCodeEnum;
import com.luobi.changgou.common.model.RestResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 统一异常处理类
 */
@Slf4j
@ControllerAdvice
public class BaseExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public RestResponse<?> error(Exception e) {
        log.error("系统出错，请检查 : {}", e.getMessage());
        return RestResponse.fail(ResponseCodeEnum.SYSTEM_ERROR, "系统繁忙，请稍后再试！");
    }

}
