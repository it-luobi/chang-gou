package com.luobi.changgou.goods.dao;

import com.luobi.changgou.goods.pojo.Sku;
import tk.mybatis.mapper.common.Mapper;

public interface SkuMapper extends Mapper<Sku> {

}
