package com.luobi.changgou.goods.dto;

import lombok.Data;

@Data
public class BrandQueryDto {

    private String name;
    private String letter;

}
