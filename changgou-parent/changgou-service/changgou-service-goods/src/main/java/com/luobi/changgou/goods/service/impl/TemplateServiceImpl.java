package com.luobi.changgou.goods.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.luobi.changgou.common.model.PageData;
import com.luobi.changgou.common.model.PageRequest;
import com.luobi.changgou.goods.dao.TemplateMapper;
import com.luobi.changgou.goods.dto.TemplateQueryDto;
import com.luobi.changgou.goods.model.TemplateModel;
import com.luobi.changgou.goods.service.TemplateService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TemplateServiceImpl implements TemplateService {

    private final TemplateMapper TemplateMapper;

    @Override
    public List<TemplateModel> listAllTemplates() {
        return TemplateMapper.selectAll();
    }

    @Override
    public TemplateModel getTemplateById(Integer id) {
        return TemplateMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<TemplateModel> listTemplateByCondition(TemplateQueryDto queryDto) {
        Example example = new Example(TemplateModel.class);
        Example.Criteria criteria = example.createCriteria();
        if (StringUtils.isNotBlank(queryDto.getName())) {
            criteria.andLike("name", "%" + queryDto.getName() + "%");
        }
        return TemplateMapper.selectByExample(example);
    }

    @Override
    public PageData<TemplateModel> listTemplateWithPageCondition(PageRequest pageRequest) {
        PageHelper.startPage(pageRequest.getPageNo(), pageRequest.getPageSize());
        Page<TemplateModel> page = (Page<TemplateModel>) TemplateMapper.selectAll();
        return new PageData<>(page.getResult(), pageRequest, page.getTotal());
    }

    @Override
    public PageData<TemplateModel> searchTemplate(TemplateQueryDto queryDto, PageRequest pageRequest) {
        Example example = new Example(TemplateModel.class);
        Example.Criteria criteria = example.createCriteria();
        if (StringUtils.isNotBlank(queryDto.getName())) {
            criteria.andLike("name", "%" + queryDto.getName() + "%");
        }

        PageHelper.startPage(pageRequest.getPageNo(), pageRequest.getPageSize());
        Page<TemplateModel> page = (Page<TemplateModel>) TemplateMapper.selectByExample(example);
        return new PageData<>(page.getResult(), pageRequest, page.getTotal());
    }

    @Override
    public void addTemplate(TemplateModel template) {
        TemplateMapper.insertSelective(template);
    }

    @Override
    public void updateTemplate(TemplateModel template) {
        TemplateMapper.updateByPrimaryKeySelective(template);
    }

    @Override
    public void deleteTemplateById(Integer id) {
        TemplateMapper.deleteByPrimaryKey(id);
    }

}
