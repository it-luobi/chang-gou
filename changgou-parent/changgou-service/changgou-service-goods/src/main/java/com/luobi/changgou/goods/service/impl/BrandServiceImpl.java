package com.luobi.changgou.goods.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.luobi.changgou.common.model.PageData;
import com.luobi.changgou.common.model.PageRequest;
import com.luobi.changgou.goods.dto.BrandQueryDto;
import com.luobi.changgou.goods.dao.BrandMapper;
import com.luobi.changgou.goods.model.BrandModel;
import com.luobi.changgou.goods.service.BrandService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service
@RequiredArgsConstructor
public class BrandServiceImpl implements BrandService {

    private final BrandMapper brandMapper;

    @Override
    public List<BrandModel> listAllBrands() {
        return brandMapper.selectAll();
    }

    @Override
    public BrandModel getBrandById(Integer id) {
        return brandMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<BrandModel> listBrandByCondition(BrandQueryDto brandDto) {
        Example example = new Example(BrandModel.class);
        Example.Criteria criteria = example.createCriteria();
        if (StringUtils.isNotBlank(brandDto.getName())) {
            criteria.andLike("name", "%" + brandDto.getName() + "%");
        }
        if (StringUtils.isNotBlank(brandDto.getLetter())) {
            criteria.andEqualTo("letter", brandDto.getLetter());
        }
        return brandMapper.selectByExample(example);
    }

    @Override
    public PageData<BrandModel> listBrandWithPageCondition(PageRequest pageRequest) {
        PageHelper.startPage(pageRequest.getPageNo(), pageRequest.getPageSize());
        Page<BrandModel> page = (Page<BrandModel>) brandMapper.selectAll();
        return new PageData<>(page.getResult(), pageRequest, page.getTotal());
    }

    @Override
    public PageData<BrandModel> searchBrand(BrandQueryDto brandDto, PageRequest pageRequest) {
        Example example = new Example(BrandModel.class);
        Example.Criteria criteria = example.createCriteria();
        if (StringUtils.isNotBlank(brandDto.getName())) {
            criteria.andLike("name", "%" + brandDto.getName() + "%");
        }
        if (StringUtils.isNotBlank(brandDto.getLetter())) {
            criteria.andEqualTo("letter", brandDto.getLetter());
        }

        PageHelper.startPage(pageRequest.getPageNo(), pageRequest.getPageSize());
        Page<BrandModel> page = (Page<BrandModel>) brandMapper.selectByExample(example);
        return new PageData<>(page.getResult(), pageRequest, page.getTotal());
    }

    @Override
    public void addBrand(BrandModel brand) {
        brandMapper.insertSelective(brand);
    }

    @Override
    public void updateBrand(BrandModel brand) {
        brandMapper.updateByPrimaryKeySelective(brand);
    }

    @Override
    public void deleteBrandById(Integer id) {
        brandMapper.deleteByPrimaryKey(id);
    }

    @Override
    public List<BrandModel> listBrandByCategoryName(String categoryName) {
        return brandMapper.listBrandByCategoryName(categoryName);
    }

}
