package com.luobi.changgou.goods.dto;

import lombok.Data;

@Data
public class SpecificationQueryDto {

    private String name;
    private String options;
    private Integer templateId;

}
