package com.luobi.changgou.goods.controller;

import com.luobi.changgou.common.model.PageData;
import com.luobi.changgou.common.model.PageRequest;
import com.luobi.changgou.common.model.RestResponse;
import com.luobi.changgou.goods.dto.TemplateQueryDto;
import com.luobi.changgou.goods.model.TemplateModel;
import com.luobi.changgou.goods.service.TemplateService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/template")
public class TemplateController {

    private final TemplateService templateService;

    /**
     * 查询所有模版列表
     */
    @GetMapping("/all")
    public RestResponse<List<TemplateModel>> listAllTemplates() {
        List<TemplateModel> allTemplates = templateService.listAllTemplates();
        return RestResponse.success(allTemplates);
    }

    /**
     * 根据id查询模版数据
     */
    @GetMapping("/{id}")
    public RestResponse<TemplateModel> getTemplateById(@PathVariable Integer id) {
        return RestResponse.success(templateService.getTemplateById(id));
    }

    /**
     * 模版列表条件查询
     */
    @GetMapping("/condition")
    public RestResponse<List<TemplateModel>> listTemplateByCondition(TemplateQueryDto queryDto) {
        return RestResponse.success(templateService.listTemplateByCondition(queryDto));
    }

    /**
     * 模版列表分页查询
     */
    @GetMapping("/page")
    public RestResponse<PageData<TemplateModel>> listTemplateWithPageCondition(PageRequest pageRequest) {
        return RestResponse.success(templateService.listTemplateWithPageCondition(pageRequest));
    }

    /**
     * 模版列表分页+条件查询
     */
    @GetMapping("/search")
    public RestResponse<PageData<TemplateModel>> searchTemplate(TemplateQueryDto queryDto, PageRequest pageRequest) {
        return RestResponse.success(templateService.searchTemplate(queryDto, pageRequest));
    }

    /**
     * 新增模版
     */
    @PostMapping("/add")
    public RestResponse<?> addTemplate(@RequestBody TemplateModel template) {
        templateService.addTemplate(template);
        return RestResponse.success();
    }

    /**
     * 修改模版
     */
    @PutMapping("/update")
    public RestResponse<?> updateTemplate(@RequestBody TemplateModel template) {
        templateService.updateTemplate(template);
        return RestResponse.success();
    }

    /**
     * 删除模版
     */
    @DeleteMapping("/{id}")
    public RestResponse<?> deleteTemplateById(@PathVariable Integer id) {
        templateService.deleteTemplateById(id);
        return RestResponse.success();
    }

}
