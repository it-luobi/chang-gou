package com.luobi.changgou.goods.dao;

import com.luobi.changgou.goods.model.BrandModel;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface BrandMapper extends Mapper<BrandModel> {

    @Select(
            "SELECT id, name, image FROM tb_brand WHERE id in (SELECT brand_id FROM tb_category_brand WHERE category_id in (SELECT id FROM tb_category WHERE name = #{name})) order by seq"
    )
    List<BrandModel> listBrandByCategoryName(@Param("name") String categoryName);

}
