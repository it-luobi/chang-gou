package com.luobi.changgou.goods.dao;

import com.luobi.changgou.goods.model.SpecificationModel;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface SpecificationMapper extends Mapper<SpecificationModel> {

    @Select(
            "SELECT id, name, options FROM tb_spec WHERE template_id in (SELECT template_id FROM tb_category WHERE name = #{categoryName}) order by seq"
    )
    List<SpecificationModel> listSpecificationByCategoryName(String categoryName);

}
