package com.luobi.changgou.goods.service;

import com.luobi.changgou.common.model.PageData;
import com.luobi.changgou.common.model.PageRequest;
import com.luobi.changgou.goods.dto.BrandQueryDto;
import com.luobi.changgou.goods.model.BrandModel;

import java.util.List;

public interface BrandService {

    /**
     * 查询所有品牌列表
     */
    List<BrandModel> listAllBrands();

    /**
     * 根据id查询品牌数据
     */
    BrandModel getBrandById(Integer id);

    /**
     * 品牌列表条件查询
     */
    List<BrandModel> listBrandByCondition(BrandQueryDto brandDto);

    /**
     * 品牌列表分页查询
     */
    PageData<BrandModel> listBrandWithPageCondition(PageRequest pageRequest);

    /**
     * 品牌列表分页+条件查询
     */
    PageData<BrandModel> searchBrand(BrandQueryDto brandDto, PageRequest pageRequest);

    /**
     * 新增品牌
     */
    void addBrand(BrandModel brand);

    /**
     * 修改品牌
     */
    void updateBrand(BrandModel brand);

    /**
     * 删除品牌
     */
    void deleteBrandById(Integer id);

    /**
     * 根据商品分类名称查询品牌列表
     */
    List<BrandModel> listBrandByCategoryName(String categoryName);

}
