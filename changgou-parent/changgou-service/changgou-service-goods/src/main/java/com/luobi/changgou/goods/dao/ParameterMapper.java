package com.luobi.changgou.goods.dao;

import com.luobi.changgou.goods.model.ParameterModel;
import tk.mybatis.mapper.common.Mapper;

public interface ParameterMapper extends Mapper<ParameterModel> {
}
