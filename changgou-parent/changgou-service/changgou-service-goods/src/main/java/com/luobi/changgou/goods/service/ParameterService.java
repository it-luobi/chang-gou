package com.luobi.changgou.goods.service;

import com.luobi.changgou.common.model.PageData;
import com.luobi.changgou.common.model.PageRequest;
import com.luobi.changgou.goods.dto.ParameterQueryDto;
import com.luobi.changgou.goods.model.ParameterModel;

import java.util.List;

public interface ParameterService {

    /**
     * 查询所有参数列表
     */
    List<ParameterModel> listAllParameters();

    /**
     * 根据id查询参数数据
     */
    ParameterModel getParameterById(Integer id);

    /**
     * 参数列表条件查询
     */
    List<ParameterModel> listParameterByCondition(ParameterQueryDto ParameterDto);

    /**
     * 参数列表分页查询
     */
    PageData<ParameterModel> listParameterWithPageCondition(PageRequest pageRequest);

    /**
     * 参数列表分页+条件查询
     */
    PageData<ParameterModel> searchParameter(ParameterQueryDto ParameterDto, PageRequest pageRequest);

    /**
     * 新增参数
     */
    void addParameter(ParameterModel Parameter);

    /**
     * 修改参数
     */
    void updateParameter(ParameterModel Parameter);

    /**
     * 删除参数
     */
    void deleteParameterById(Integer id);

}
