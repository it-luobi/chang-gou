package com.luobi.changgou.file;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class FileApplication {

    // http://localhost:9008/file/swagger-ui/index.html
    public static void main(String[] args) {
        SpringApplication.run(FileApplication.class, args);
    }

}
