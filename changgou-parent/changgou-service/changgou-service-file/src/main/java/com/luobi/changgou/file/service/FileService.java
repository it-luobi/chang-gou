package com.luobi.changgou.file.service;

import com.luobi.changgou.file.model.FastDFSFile;
import com.luobi.changgou.file.model.FileOperationDto;
import com.luobi.changgou.file.util.FastDFSClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.csource.fastdfs.FileInfo;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Objects;

@Slf4j
@Service
@RequiredArgsConstructor
public class FileService {

    public String uploadFile(MultipartFile file) {
        // 判断文件是否存在
        if (file == null) {
            throw new RuntimeException("文件不存在！");
        }
        // 获取文件的完整名称
        String originalFilename = file.getOriginalFilename();
        if (StringUtils.isEmpty(originalFilename)) {
            throw new RuntimeException("文件不存在！");
        }

        // 获取文件的扩展名
        String extName = originalFilename.substring(originalFilename.lastIndexOf(".") + 1);

        // 获取文件内容
        byte[] content;
        try {
            content = file.getBytes();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return FastDFSClient.uploadFile(new FastDFSFile(originalFilename, content, extName));
    }

    public void downloadFile(FileOperationDto dto, HttpServletResponse response) {
        InputStream in = FastDFSClient.downloadFile(dto.getGroupName(), dto.getRemoteFileName());
        if (in == null) {
            throw new RuntimeException("文件不存在！");
        }

        try {
            OutputStream out = response.getOutputStream();
            IOUtils.copy(in, out);
        } catch (IOException e) {
            log.error("文件下载失败. {}", dto.getRemoteFileName());
            throw new RuntimeException("文件下载失败.", e);
        }

    }

    public Integer deleteFile(FileOperationDto dto) {
        Integer result;
        try {
            result = FastDFSClient.deleteFile(dto.getGroupName(), dto.getRemoteFileName());
        } catch (Exception e) {
            log.error("文件删除失败. {}", dto.getRemoteFileName());
            throw new RuntimeException("文件删除失败.", e);
        }
        return result;
    }

    public FileInfo getFileInfo(FileOperationDto dto) {
        if (Objects.isNull(dto)) {
            throw new RuntimeException("参数不能为空！");
        }
        return FastDFSClient.getFileInfo(dto.getGroupName(), dto.getRemoteFileName());
    }

}
