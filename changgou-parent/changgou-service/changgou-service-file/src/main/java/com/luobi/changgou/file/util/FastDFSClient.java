package com.luobi.changgou.file.util;

import com.luobi.changgou.file.model.FastDFSFile;
import org.csource.common.NameValuePair;
import org.csource.fastdfs.ClientGlobal;
import org.csource.fastdfs.FileInfo;
import org.csource.fastdfs.ServerInfo;
import org.csource.fastdfs.StorageClient;
import org.csource.fastdfs.StorageServer;
import org.csource.fastdfs.TrackerClient;
import org.csource.fastdfs.TrackerServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;

public class FastDFSClient {

    private static Logger logger = LoggerFactory.getLogger(FastDFSClient.class);
    private static final String CONF_FILENAME = "fdfs_client.conf";

    /***
     * 初始化加载 FastDFS 的 TrackerServer 配置
     */
    static {
        try {
            String filePath = URLDecoder.decode(new ClassPathResource(CONF_FILENAME).getFile().getAbsolutePath(), "UTF-8");
            ClientGlobal.init(filePath);
        } catch (Exception e) {
            logger.error("FastDFS Client init fail!", e);
        }
    }

    /***
     * 文件上传
     * @param file
     * @return 文件的存储路径
     */
    public static String uploadFile(FastDFSFile file) {
        // 获取文件的作者
        NameValuePair[] meta_list = new NameValuePair[1];
        meta_list[0] = new NameValuePair("author", file.getAuthor());

        // 接收返回数据
        String[] uploadResults = null;
        StorageClient storageClient;
        try {
            // 创建 StorageClient 客户端对象
            storageClient = getStorageClient();
            // 文件上传 : 文件字节数组、文件扩展名、文件作者
            uploadResults = storageClient.upload_file(file.getContent(), file.getExt(), meta_list);
        } catch (Exception e) {
            logger.error("Exception occurred while uploading the file: " + file.getName(), e);
        }

        if (uploadResults == null) {
            logger.error("Failed to upload file.");
            return "Failed to upload file.";
        }

        // 获取组名
        String groupName = uploadResults[0];
        // 获取文件存储路径
        String remoteFileName = uploadResults[1];

        String fileUrl;
        try {
            fileUrl = FastDFSClient.getTrackerUrl() + "/" + groupName + "/" + remoteFileName;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return fileUrl;
    }

    /***
     * 文件下载
     * @param groupName
     * @param remoteFileName
     * @return
     */
    public static InputStream downloadFile(String groupName, String remoteFileName) {
        try {
            // 创建StorageClient
            StorageClient storageClient = getStorageClient();

            // 下载文件
            byte[] fileByte = storageClient.download_file(groupName, remoteFileName);
            return new ByteArrayInputStream(fileByte);
        } catch (Exception e) {
            logger.error("Exception: Failed to download file from FastDFS.", e);
        }

        return null;
    }

    /***
     * 文件删除
     * @param groupName
     * @param remoteFileName
     * @throws Exception
     */
    public static Integer deleteFile(String groupName, String remoteFileName) throws Exception {
        // 创建StorageClient
        StorageClient storageClient = getStorageClient();

        // 删除文件
        return storageClient.delete_file(groupName, remoteFileName);
    }

    /***
     * 获取文件信息
     * @param groupName:组名
     * @param remoteFileName：文件存储完整名
     * @return
     */
    public static FileInfo getFileInfo(String groupName, String remoteFileName) {
        try {
            StorageClient storageClient = getStorageClient();
            return storageClient.get_file_info(groupName, remoteFileName);
        } catch (Exception e) {
            logger.error("Exception: Failed to get file info from FastDFS.", e);
        }
        return null;
    }

    /***
     * 获取Storage组
     * @param groupName
     * @return
     * @throws IOException
     */
    public static StorageServer[] getStoreStorages(String groupName) throws IOException {
        // 创建TrackerClient
        TrackerClient trackerClient = new TrackerClient();
        // 获取TrackerServer
        TrackerServer trackerServer = trackerClient.getConnection();
        // 获取Storage组
        return trackerClient.getStoreStorages(trackerServer, groupName);
    }

    /***
     * 获取Storage信息,IP和端口
     * @param groupName
     * @param remoteFileName
     * @return
     * @throws IOException
     */
    public static ServerInfo[] getFetchStorages(String groupName, String remoteFileName) throws IOException {
        TrackerClient trackerClient = new TrackerClient();
        TrackerServer trackerServer = trackerClient.getConnection();
        return trackerClient.getFetchStorages(trackerServer, groupName, remoteFileName);
    }

    /***
     * 获取Tracker服务地址
     */
    public static String getTrackerUrl() throws IOException {
        String ip = getTrackerServer().getInetSocketAddress().getHostString();
        int port = ClientGlobal.getG_tracker_http_port();
        return "http://" + ip + ":" + port;
    }

    /***
     * 获取Storage客户端
     */
    private static StorageClient getStorageClient() throws IOException {
        TrackerServer trackerServer = getTrackerServer();
        return new StorageClient(trackerServer, null);
    }

    /***
     * 获取Tracker
     */
    private static TrackerServer getTrackerServer() throws IOException {
        TrackerClient trackerClient = new TrackerClient();
        return trackerClient.getConnection();
    }

}
