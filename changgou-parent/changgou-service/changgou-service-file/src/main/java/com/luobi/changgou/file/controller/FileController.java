package com.luobi.changgou.file.controller;

import com.luobi.changgou.common.model.RestResponse;
import com.luobi.changgou.file.model.FileOperationDto;
import com.luobi.changgou.file.service.FileService;
import lombok.RequiredArgsConstructor;
import org.csource.fastdfs.FileInfo;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import static com.luobi.changgou.common.model.ResponseCodeEnum.OPERATION_FAILED;
import static com.luobi.changgou.common.model.ResponseCodeEnum.PARAMETER_IS_NULL;
import static com.luobi.changgou.common.model.ResponseCodeEnum.REJECT_OPERATION;

@RestController
@RequiredArgsConstructor
@RequestMapping("/file-operation")
public class FileController {

    private final FileService fileService;
    private static final Long FILE_SIZE_LIMIT = 20 * 1024 * 1024L;

    @PostMapping("/upload")
    public RestResponse<String> uploadFile(MultipartFile file) {
        if (file == null) {
            return RestResponse.fail(PARAMETER_IS_NULL, "文件不能为空！");
        }
        if (file.getSize() > FILE_SIZE_LIMIT) {
            return RestResponse.fail(REJECT_OPERATION, "文件不能超过20M！");
        }
        String fileUrl = fileService.uploadFile(file);
        return RestResponse.success(fileUrl);
    }

    @GetMapping("/download")
    public void downloadFile(@Valid FileOperationDto dto, HttpServletResponse response) {
        fileService.downloadFile(dto, response);
    }

    @DeleteMapping("/delete")
    public RestResponse<?> deleteFile(@RequestBody @Valid FileOperationDto dto) {
        Integer result = fileService.deleteFile(dto);
        return result == 0 ? RestResponse.success() : RestResponse.fail(OPERATION_FAILED, "文件删除失败！");
    }

    @GetMapping("/info")
    public RestResponse<FileInfo> getFileInfo(@Valid FileOperationDto dto) {
        return RestResponse.success(fileService.getFileInfo(dto));
    }

}
