package com.luobi.changgou.file.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class FileOperationDto {

    @Schema
    @NotBlank
    private String groupName;
    @Schema
    @NotBlank
    private String remoteFileName;

}
